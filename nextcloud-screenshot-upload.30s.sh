#!/bin/bash

#  <xbar.title>Nextcloud Screenshot Uploader</xbar.title>
#  <xbar.version>v1.0</xbar.version>
#  <xbar.author>A.Gurcan Ozturk</xbar.author>
#  <xbar.author.github>gurcanozturk</xbar.author.github>
#  <xbar.desc>Take screenshot with selection, upload to Nextcloud, copy share link to clipboard</xbar.desc>
#  <xbar.image>http://www.hosted-somewhere/pluginimage</xbar.image>
#  <xbar.dependencies>bash</xbar.dependencies>
#  <xbar.abouturl>http://gurcanozturk.com/</xbar.abouturl>

EXDATE="$(date -v+7d +%Y-%m-%d)"
DATETIME="$(date +%Y%m%d-%H%M%S)"

NC_URL="https://nextcloud.example.com"
NC_USER="nextcloud-user"
NC_PASS="nextcloud-user-password"

FILE_NAME="screenshot_$DATETIME.jpg"
FILE_PATH="Screenshots"
UPLOAD_URL="/remote.php/dav/files/$NC_USER/$FILE_PATH"
SHARELINK_URL="ocs/v1.php/apps/files_sharing/api/v1/shares"

if [[ "$1" == "copy" ]];
then
    echo $2 | pbcopy
elif [[ "$1" == "clear" ]];
then
    echo "" | pbcopy
fi

case "$1" in
    shot)
        # capture screen with selection
        screencapture -T 0 -t jpeg -i -Jselection $FILE_NAME

        # do upload
        curl -u $NC_USER:$NC_PASS -X PUT $NC_URL/$UPLOAD_URL/$FILE_NAME -T $FILE_NAME

        # create and print share link for uploaded file and print
        SHARE_LINK=$(curl -sS -u $NC_USER:$NC_PASS -H "OCS-APIRequest: true" -X POST $NC_URL/$SHARELINK_URL -d path="$FILE_PATH/$FILE_NAME" -d shareType=3 -d permissions=1 -d expireDate=$EXDATE | tr \\n \\r | sed 's:.*<url>\([^<]*\)<.*:\1:' | tr \\r \\n;echo "")
        if [[ "$SHARE_LINK" != "" ]];
        then 
          echo $SHARE_LINK | sed 's/http/https/' | pbcopy
        fi
        rm $FILE_NAME
        ;;
esac

    echo "🍺"
    echo '---'
    echo "Take Screenhot | bash='$0' param1=shot terminal=false refresh=true"
    echo '---'
    echo "Copy Link: $(pbpaste) |bash='$0' param1=copy param2=$(pbpaste) refresh=true terminal=false"
    exit
