This plugin helps to take screenshot, upload to Nextcloud instance and give share link. Written in bash and no need to special library, application etc.


You need to have xbar (https://xbarapp.com) installed on your Mac OS X and replaced Nextcloud credentials.

Put python script into $HOME/Library/Application Support/xbar/plugins folder then click Refresh from any xbar logo on menu bar or restart xbar.
